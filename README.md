# Usage

This assumes that you are using go mod to build.

```shell
docker run --rm -v "$PWD":/myrepo -w /myrepo lunny/centos-go-nodejs:latest go build
```